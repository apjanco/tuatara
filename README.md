# tuatara   <img height=50 src="tuatara.png" />


### A IIIF presentation manifest and collection management tool. 

The International Image Interoperability Framework (IIIF) provides common standards for publishing images, audio and video with metadata to the web.
- This makes it possible to store information about an item, or collection of items, in a single manifest.
- Annotations, transcriptions, 

## Useage

- Create a new collection manifest
- Add items to the collection with image files (canvas)
- Images and manifests are saved to disk and served with Cantaloupe

Direct access to image files: http://tuatara.pennds.org/image/playbill.jpg
or IIIF image server https://tuatara.pennds.org/iiif/3/playbill.jpg/full/max/0/default.jpg

Universal Viewer
https://uv-v3.netlify.app/#?c=&m=&s=&cv=&manifest=https%3A%2F%2Ftuatara.pennds.org%2Fmanifest%2Fexample.json&xywh=-345%2C50%2C1329%2C845

OpenSeaDragon 
https://greenseer.pennds.org/?manifest=https://tuatara.pennds.org/manifest/bull-frog.json

TODO
- provide link to manifests 
- nginx config for /images, just get the image, no IIIF
- Code editor for json files
- IIIF validation 

It is not enought to make it simple to publish simple images, it must also make use of IIIF's capabilities, 
otherwise, there are better options (S3) https://docs.aws.amazon.com/AmazonS3/latest/userguide/UsingMetadata.html
