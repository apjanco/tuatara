import srsly
import uuid
import httpx
from pathlib import Path
from fastapi import FastAPI, Request, Form, File, UploadFile
from fastapi.templating import Jinja2Templates
from fastapi.staticfiles import StaticFiles
from fastapi.responses import HTMLResponse
from typing import Optional
from slugify import slugify

settings = srsly.read_yaml('settings.yml')
templates = Jinja2Templates(directory="templates")
manifest_directory = Path(settings['manifest_directory'])
assert manifest_directory.exists()
image_directory = Path(settings['image_directory'])
assert image_directory.exists()

app = FastAPI()
app.mount("/assets", StaticFiles(directory="assets"), name="assets")


def load_manifests():
    collections = []
    collection_names = []
    items = []

    try:
        for i, manifest in enumerate(Path(settings['manifest_directory']).iterdir()):
            data = srsly.read_json(manifest)
            if "Collection".lower() in data.get('type', '').lower():
                collections.append(data)
    except Exception as e:
        print(e)

    for collection in collections:
        collection_names.append(collection['label']['en'][0])
        for item in collection.get('items', []):
            item['collection'] = collection.get('label')
            items.append(item)
    return collections, collection_names, items

def item_collection(uuid:str):
    """A util function to identify an item's home collection"""
    collections, _, _ = load_manifests()
    for collection in collections:
        item_match = next(item for item in collection["items"] if item["uuid"] == uuid)
        if item_match:
            return collection

@app.get("/")
async def root(request: Request):
        _, collection_names, items = load_manifests()
        return templates.TemplateResponse("index.html", {"request": request, "collection_names":collection_names,"items":items})

@app.post("/add_collection")
async def add_collection(request: Request, label: str = Form(), summary: Optional[str] = Form(None)):
   
    slug = slugify(label)
    m = {}
    m["@context"] = "http://iiif.io/api/presentation/3/context.json"
    #The URI that identifies the resource. ex. https://blinky.pennds.org/iiif/3/{identifier}/info.json
    m["id"] =f"{settings['iiif_server']}/manifest/{slug}" #TODO how does cantaloupe serve collection manifests?
    m["uuid"] = str(uuid.uuid4())
    m["slug"] = slug
    m["type"] = "Collection"
    m["label"] = { "en": [ f"{label}" ] }
    if summary:
        m["summary"] = { "en": [ f"{summary}" ] }

    m["items"] = []
    srsly.write_json((manifest_directory / (slug +'.json') ), m)
    # update the page to reflect the new collection
    _, collection_names, items = load_manifests()
    return templates.TemplateResponse("index.html", {"request": request, "collection_names":collection_names,"items":items, "collection_created":True})


@app.post('/add')
async def new_item(request: Request, label: str = Form(), collection: str = Form(), summary: Optional[str] = Form(None), images: list[UploadFile] = File(None)):
    slug = slugify(collection)
    collection_manifest = manifest_directory / (slug +'.json')
    if collection_manifest.exists():
        # Manifest
        m ={}
        m["@context"] = "http://iiif.io/api/presentation/3/context.json"
        m["id"] = f"{settings['iiif_server']}/manifest/{slugify(label)}.json"
        m["uuid"] = str(uuid.uuid4())
        m["slug"] = slugify(label)
        m["type"] = "Manifest"
        m["label"] = { "en": [ f"{label}" ] }
        m["items"] = []
        if images[0].filename:
            #TODO add thumbnail
            for file in images:
                filename = slugify(file.filename)
                contents = file.file.read()
                #TODO handle file exists/duplicate
                (image_directory / filename).write_bytes(contents)
                image_json = httpx.get(f"{settings['iiif_server']}/iiif/3/{filename}/info.json")
                assert image_json.status_code == 200
                is3 = image_json.json()
                # Canvas
                c = {}
                c["id"] = f"{settings['iiif_server']}/manifest/{slugify(label)}.json"
                c["label"] = { "en": [ f"{file.filename}" ] }
                c["type"] ="Canvas"
                c["height"] =  is3['height']
                c["width"] = is3['width']
                c["items"] = []
                # AnnotationPage
                ap = {}
                ap["id"] = f"{settings['iiif_server']}/manifest/{slugify(label)}.json"
                ap["type"] ="AnnotationPage"
                ap["items"] = []
                # Annotation
                # Image
                f"{settings['iiif_server']}/iiif/3/{filename}"
                
                anno = {}
                anno["id"] = f"{settings['iiif_server']}/manifest/{slugify(label)}"
                anno["type"] = "Annotation"
                anno["motivation"] = "painting"
                anno["target"] = f"{settings['iiif_server']}/manifest/{slugify(label)}.json"
                anno["body"] = {}
                anno["body"]["id"] = f"https://tuatara.pennds.org/iiif/3/{filename}/full/max/0/default.jpg"
                anno["body"]["type"] = "Image"
                anno["body"]["format"] = file.content_type
                anno["body"]["height"] = is3['height']
                anno["body"]["width"] = is3['width']
                anno["body"]["service"] = []
                anno["body"]["service"].append(is3)
                  
                ap["items"].append(anno)
                c["items"].append(ap)
                m['items'].append(c)
        
        

        # update collection manifest
        collection_json = srsly.read_json(collection_manifest)
        collection_json['items'].append(m)
        srsly.write_json((manifest_directory / (slug +'.json') ), collection_json)

        # write manifest file
        m["thumbnail"] = ''
        srsly.write_json((manifest_directory / f"{slugify(label)}.json" ), m)

        _, collection_names, items = load_manifests()
        return templates.TemplateResponse("index.html", {"request": request, "collection_names":collection_names,"items":items, "item_created":True})
    else:
        _, collection_names, items = load_manifests()
        return templates.TemplateResponse("index.html", {"request": request, "collection_names":collection_names,"items":items, "item_error":True})


@app.post('/existing_item')
async def existing_item(request: Request, uri: str = Form(), collection: str = Form()):
    manifest = httpx.get(uri)
    if manifest.status_code == 200: 
        manifest = manifest.json()
        if manifest.get('id', None) and manifest.get("type", '') == "Manifest": # assert Manifest and IIIF 3, 2 would have @id
            slug = slugify(collection)
            collection_manifest = manifest_directory / slug
            collection_json = srsly.read_json(collection_manifest)
            collection_json['items'].append(manifest)
            srsly.write_json((manifest_directory / slug ), collection_json)
            _, collection_names, items = load_manifests()
            return templates.TemplateResponse("index.html", {"request": request, "collection_names":collection_names,"items":items, "existing_created":True})
        else:
            _, collection_names, items = load_manifests()
            return templates.TemplateResponse("index.html", {"request": request, "collection_names":collection_names,"items":items, "iiif_error":True})

    else:
        _, collection_names, items = load_manifests()
        return templates.TemplateResponse("index.html", {"request": request, "collection_names":collection_names,"items":items, "existing_error":True})

@app.get('/edit_item/{uuid}')
async def edit_item(request: Request, uuid:str, response_class=HTMLResponse):
    _, collection_names, items = load_manifests()
    manifest = next(item for item in items if item["uuid"] == uuid)
    collection = item_collection(uuid)
    return templates.TemplateResponse("item_modal.html", {"request": request, "collection":collection,"manifest":manifest, "collection_names":collection_names})

@app.get('/edit_code/{uuid}')
async def edit_item(request: Request, uuid:str, response_class=HTMLResponse):
    _, collection_names, items = load_manifests()
    manifest = next(item for item in items if item["uuid"] == uuid)
    return templates.TemplateResponse("item_code.html", {"request": request,"manifest":manifest})


@app.post('/update_item/{uuid}')
async def edit_item(request: Request, uuid:str, response_class=HTMLResponse):
    _, collection_names, items = load_manifests()
    manifest = next(item for item in items if item["uuid"] == uuid)
    print(manifest)
    return templates.TemplateResponse("item_modal.html", {"request": request})
